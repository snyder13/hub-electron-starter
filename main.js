// spec for main window
const MAIN = {
	'width': 800,
	'height': 600
};

require('electron-debug')();
const { app, BrowserWindow } = require('electron');

// hot-reloading. run `yarn watch` in another terminal to re-build bundle
if (process.env.NODE_ENV && /^dev/i.test(process.env.NODE_ENV)) {
	require('electron-reload')(
		`${ __dirname }/dist`,
		{ 'electron': require(`${ __dirname }/node_modules/electron`) }
	);
}

let mainWindow;

app.on('ready', () => {
	mainWindow = new BrowserWindow(MAIN);
	mainWindow.setMenu(null);
	mainWindow.loadFile('index.html');

	mainWindow.on('closed', () => mainWindow = null)
});


app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit()
	}
});

app.on('activate', () => {
	if (mainWindow === null) {
		createWindow()
	}
})
