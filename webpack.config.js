const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
	'target': 'electron-renderer',
	'mode': 'none',
	'entry': './src',
	'output': {
		'path': path.resolve('./static'),
		'filename': 'bundle.js'
	},
	'plugins': [
		new CopyWebpackPlugin([
			{
				'from': 'assets/**',
				'context': path.resolve('./src'),
				'to': path.resolve('./static')
			}
		])
	],
	'module': {
		'rules': [{
			'test': /\.lit$/,
			'use': [
				{
					'loader': 'babel-loader',
					'options': {
						'presets': [
							[
								'@babel/preset-env', {
									'targets': {
										'browsers': [ 'safari 11' ]
									}
								}
							]
						]
					}
				},
				{ 'loader': 'lit-loader' }
			]
		}]
	}
};
