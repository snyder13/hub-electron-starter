# hub-electron-starter

This repo is intended to set up a (relatively) comfy work environment for building cross-platform apps with Electron.

## Dependencies

 * `node.js` (tested with v10)
 * `yarn` - `npm install -g yarn` -- recommended, `npm` untested

## Installation

 * `yarn install`

## Running
 * `yarn build` - `webpack` the source once, or `yarn watch`, `webpack` and then wait for changes and re-build automatically
 * `yarn start`

![exciting action shot](src/assets/screenshot.png)

The application demonstrates spawning windows and communicating between them. You can do any other stuff you could do in a normal web context besides, and for other native APIs I recommend having a look at https://github.com/hokein/electron-sample-apps

The Electron dev tools are included. To use them, enter in the inspector console: `require('devtron').install()`. A new inspector tab will appear with some custom utilities.

Feel free to file an issue w/ any questions.

## Packaging
 * `yarn dist`

This is barely tested for linux->linux builds. More needs to be done to support OS X in particular.

## Files

 * `main.js` - Entry point. Launches the first window. See here to adjust the characteristics of that window (size, menu settings [hidden by default])
 * `index.html` - Loaded in the main window. You're clear to do whatever web stuff you'd like here. By default it loads the webpacked bundle in `static/`, built from `src/`. By some means, you ought to link to another script to serve as your renderer.
 * `count.html` - Another entry point for the example app, which spawns child windows. It's the same as `index.html` except that it invokes a different web component.
 * `package.json` - In addition to the usual stuff, this also defines how packaging works. Or it will, anyway. Currently there's not much there.
 * `src/index.js` - This is the `webpack` entry point. Everything you want in the bundle should be linked either directly (with an `import` here) or indirectly (`import`-ed by something that was `import`-ed here). It's cleanest if this `import`s components that referenced directly in the HTML entry points only, and those components can `import` anything they depend on. Alternatively, if you don't want to use `lit-element`, treat it as any other `<script>` source and do whatever you'd like here.
 * `src/components/` - The web components live here. `lit-loader` converts the single-file component format into something usable by Electron during the build stage.
 * `src/assets/` - Place anything you'd like here to get it copied to the `static/` folder for use in the application.
 * `static/` - Files that are used during runtime by the application. Though you can reference anything on the file system it's important the stuff lives here for the packaging stage.
 * `dist/` - Packages resulting from `yarn dist` go here.
 * `postcss.config.js` - PostCSS configuration. Minimal by default, just allows `@import`-ing things. There are [many other options](https://github.com/postcss/postcss)
 * `webpack.config.js` - Accomplishes the single-file component reformatting and asset-copying mentioned above. You can add plugins and rules to add support for things like TypeScript, or alternative markup languages.

## TODO

 * Electron has a nice auto-updater thing, which I should figure out
 * Packaging improvements - verify OS X and Win builds, go through target options to find out which are needed, see if there's still a way to do "portable" builds rather than installers
 * Add headless tests

